package viali.pl.ekonomik.presentationalfa;

import android.graphics.drawable.Drawable;
import android.hardware.display.DisplayManager;
import android.os.Bundle;
import android.app.Activity;
import android.app.Presentation;
import android.content.Context;
import android.os.Vibrator;
import android.util.Log;
import android.util.SparseArray;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;
import android.widget.CompoundButton.OnCheckedChangeListener;
import java.util.Random;

public class MainActivity extends Activity {

    private DisplayManager mDisplayManager;
    private DisplayListAdapter mDisplayListAdapter;
    private ListView mListView;
    private final SparseArray<RemotePresentation> mActivePresentations = new SparseArray<RemotePresentation>();
    private ImageButton btn1, btn2, btn3, btn4, btn5;
    private Vibrator vibrator;
    private VibratePatterns vibratePatterns;
    private int smilyK = 0;


    private final DisplayManager.DisplayListener mDisplayListener = new DisplayManager.DisplayListener() {
        @Override
        public void onDisplayAdded(int displayId) {
            mDisplayListAdapter.updateContents();
        }

        @Override
        public void onDisplayChanged(int displayId) {
            mDisplayListAdapter.updateContents();
        }

        @Override
        public void onDisplayRemoved(int displayId) {
            mDisplayListAdapter.updateContents();
        }
    };
    private Display[] presentationDisplays;
    private int mainRemoteDisplay;
    private boolean smilyOn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.test);

        vibratePatterns = new VibratePatterns();

        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);
        btn3 = findViewById(R.id.btn3);
        btn4 = findViewById(R.id.btn4);
        btn5 = findViewById(R.id.btn5);


        mDisplayManager = (DisplayManager) getSystemService(Context.DISPLAY_SERVICE);

        mDisplayListAdapter = new DisplayListAdapter(this);
        mListView = findViewById(R.id.display_list);
        mListView.setAdapter(mDisplayListAdapter);

        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        setListeners();

    }

    private void setListeners() {

        btn1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("onTouch", String.valueOf(event));
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    Log.d("Touch", "VIBRATE~!!!!");
                    vibrate(1);
                    dostuff(1);
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    vibrator.cancel();
                    clearRemoteScreen();
                    smilyFace();

                }
                return false;
            }
        });


        btn2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("onTouch", String.valueOf(event));
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    Log.d("Touch", "VIBRATE~!!!!");
                    vibrate(2);
                    dostuff(2);
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    vibrator.cancel();
                    clearRemoteScreen();
                    smilyFace();
                }
                return false;
            }
        });


        btn3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("onTouch", String.valueOf(event));
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    Log.d("Touch", "VIBRATE~!!!!");
                    vibrate(3);
                    dostuff(3);
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    vibrator.cancel();
                    clearRemoteScreen();
                    smilyFace();
                }
                return false;
            }
        });


        btn4.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("onTouch", String.valueOf(event));
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    Log.d("Touch", "VIBRATE~!!!!");
                    vibrate(4);
                    dostuff(4);
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    vibrator.cancel();
                    clearRemoteScreen();
                    smilyFace();
                }
                return false;
            }
        });


        btn5.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("onTouch", String.valueOf(event));
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    Log.d("Touch", "VIBRATE~!!!!");
                    vibrate(5);
                    dostuff(5);
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    vibrator.cancel();
                    clearRemoteScreen();
                    smilyFace();
                }
                return false;
            }
        });

    }

    private void smilyFace() {
        if (smilyK < 69) {
            smilyK++;
        } else {
            smilyK = 0;
        }

        Random rn = new Random();
        int random = rn.nextInt(9) + 1;
        Log.d("SMILY", "Smily: " + smilyK + ", Random: " + random);

        if (smilyOn) {
            btn1.setBackground(this.getResources().getDrawable(R.drawable.btn));
            btn2.setBackground(this.getResources().getDrawable(R.drawable.btn));
            btn3.setBackground(this.getResources().getDrawable(R.drawable.btn));
            btn4.setBackground(this.getResources().getDrawable(R.drawable.btn));
            btn5.setBackground(this.getResources().getDrawable(R.drawable.btn));
            smilyOn = false;
        } else {
            if (smilyK % 10 == random) {
                // Smily function

                smilyOn = true;

                btn1.setBackground(this.getResources().getDrawable(R.drawable.btn_smily));
                btn2.setBackground(this.getResources().getDrawable(R.drawable.btn_smily));
                btn3.setBackground(this.getResources().getDrawable(R.drawable.btn_smily));
                btn4.setBackground(this.getResources().getDrawable(R.drawable.btn_smily));
                btn5.setBackground(this.getResources().getDrawable(R.drawable.btn_smily));
            }
        }

    }

    private void clearRemoteScreen() {
        if (mActivePresentations.size() == 1) {
            mActivePresentations.get(mainRemoteDisplay).stopAnimation();
        }


    }

    private void vibrate(int i) {

        Log.d("VIBRATE", "InVIbrate");

        if (vibrator.hasVibrator()) {

            Log.d("VIBRATE", "do Wibrate");

            vibrator.vibrate(3000); // for 500 ms
            vibrator.vibrate(vibratePatterns.getVibe(i), 0);
        }
    }

    private void dostuff(int i) {
        Log.d("TAG", "Clicked");
        presentationDisplays = mDisplayManager.getDisplays(DisplayManager.DISPLAY_CATEGORY_PRESENTATION);
        if (presentationDisplays.length > 0) {
            // If there is more than one suitable presentation display, then we could consider
            // giving the user a choice.  For this example, we simply choose the first display
            // which is the one the system recommends as the preferred presentation display.
            showPresentation(presentationDisplays[0], i);
        }
    }

    protected void onResume() {
        super.onResume();
        mDisplayListAdapter.updateContents();
        mDisplayManager.registerDisplayListener(mDisplayListener, null);
    }

    private void showPresentation(Display display, int i) {
        RemotePresentation presentation = new RemotePresentation(this, display);
        mActivePresentations.put(display.getDisplayId(), presentation);
        mainRemoteDisplay = display.getDisplayId();
        presentation.show();
        presentation.move(i);
    }

    private void hidePresentation(Display display) {
        final int displayId = display.getDisplayId();
        RemotePresentation presentation = mActivePresentations.get(displayId);
        if (presentation == null) {
            return;
        }

        presentation.dismiss();
        mActivePresentations.delete(displayId);
    }

    private final class DisplayListAdapter extends ArrayAdapter<Display> {
        final Context mContext;
        private OnCheckedChangeListener mCheckedRemoteDisplay = new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton view, boolean isChecked) {
                synchronized (mCheckedRemoteDisplay) {
                    final Display display = (Display) view.getTag();
                    if (isChecked) {
                        showPresentation(display, 0);
                    } else {
                        hidePresentation(display);
                    }
                }
            }
        };

        public DisplayListAdapter(Context context) {
            super(context, R.layout.list_item);
            mContext = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final View v;
            if (convertView == null) {
                v = ((Activity) mContext).getLayoutInflater().inflate(R.layout.list_item, null);
            } else {
                v = convertView;
            }

            final Display display = getItem(position);

            //TITLE
            TextView tv = v.findViewById(R.id.display_id);
            tv.setText(display.getName() + "( ID: " + display.getDisplayId() + " )");

            //DESCRIPTION
            tv = v.findViewById(R.id.display_desc);
            tv.setText(display.toString());

            //SHOW or HIDE the presentation
            CheckBox cb = v.findViewById(R.id.display_cb);
            cb.setTag(display);
            cb.setOnCheckedChangeListener(mCheckedRemoteDisplay);
            return v;
        }

        public void updateContents() {
            clear();

            Display[] displays = mDisplayManager.getDisplays();
            addAll(displays);
        }
    }

    private final class RemotePresentation extends Presentation {
        ImageView imageView;
        private Animation rotation;


        public RemotePresentation(Context context, Display display) {
            super(context, display);
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.remote_display);
            imageView = findViewById(R.id.ivRemote);
            rotation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.rotate_1);
        }

        public void rotate(int i, Drawable drawable) {

            imageView.setColorFilter(this.getResources().getColor(R.color.blue));
            imageView.clearAnimation();
            imageView.setImageDrawable(drawable);
            rotation.setFillAfter(true);
            imageView.startAnimation(rotation);
        }

        public void stopAnimation() {
            imageView.clearAnimation();
            imageView.setImageDrawable(this.getResources().getDrawable(R.drawable.black));
            imageView.setColorFilter(this.getResources().getColor(R.color.black));
        }

        public void move(int x) {

            switch (x) {
                case 1:
                    Log.d("REMOTE", "1");
                    rotate(x, this.getResources().getDrawable(R.drawable.g1));
                    break;
                case 2:
                    Log.d("REMOTE", "2");
                    rotate(x, this.getResources().getDrawable(R.drawable.g2));
                    break;
                case 3:
                    Log.d("REMOTE", "3");
                    rotate(x, this.getResources().getDrawable(R.drawable.g3));
                    break;
                case 4:
                    Log.d("REMOTE", "4");
                    rotate(x, this.getResources().getDrawable(R.drawable.g4));
                    break;
                case 5:
                    Log.d("REMOTE", "5");
                    rotate(x, this.getResources().getDrawable(R.drawable.g5));
                    break;
                default:
                    Log.d("REMOTE", "0");
                    break;

            }
        }
    }
}



