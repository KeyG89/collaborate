package viali.pl.ekonomik.presentationalfa;

import java.util.HashMap;

public class VibratePatterns {
    private long[] mVibratePattern1 = {0, 500, 100, 500, 100, 500};
    private long[] mVibratePattern2 = {0, 50, 25, 50, 25, 50};
    private long[] mVibratePattern3 = {0, 20, 50, 200, 50, 10};
    private long[] mVibratePattern4 = {0, 35, 100, 50, 300, 50};
    private long[] mVibratePattern5 = {0, 75, 25, 25, 100, 50};
    private  HashMap<Integer, long[]> vMap = new HashMap<Integer, long[]>();

    VibratePatterns() {
        vMap.put(1, mVibratePattern1);
        vMap.put(2, mVibratePattern2);
        vMap.put(3, mVibratePattern3);
        vMap.put(4, mVibratePattern4);
        vMap.put(5, mVibratePattern5);

    }

    public long[] getVibe(int i) {
        return vMap.get(i);
    }
}
